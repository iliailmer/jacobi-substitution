kernelopts(printbytes=false, assertlevel=1):
interface(echo=0, prettyprint=0):
read "imports/generate_poly_system.mpl":
read "imports/bfs_deriv.mpl":
read "imports/create_substitutions.mpl":

sigma := [
 diff(x1(t),t) = a*x1(t),
 diff(x2(t),t) = b*x2(t) + c*x1(t),
 y1(t) = a+b+c,
 y2(t) = a
]:
